import {Chalk} from "chalk";
import {Inquirer} from "inquirer";
import {Cyan, Documentation, DocUsage, Glob, IAutoInquire, IAutoMapper, IExecute,} from "./Typings";

export = async function (folderName: string, chalk: Chalk, inquirer: Inquirer, autoInquirer: IAutoInquire, autoMap: IAutoMapper, execute: IExecute): Promise<Cyan> {
	let ide: any = {
		vs2017: "VS 2017",
		webStorm: "Rider",
		editor: "Text Editor (VS Code, Sublime, Atom etc)"
	};
	
	let ideAnswer: object = await autoInquirer.InquireAsList(ide, "Which IDE do you use?");
	let ideChoice: string = autoMap.ReverseLoopUp(ide, ideAnswer);
	
	let globs: Glob[] = [];
	
	let ideIgnore: { vs2017: string[], webStorm: string[] } = {
		vs2017: ["**/.idea/**/*", "**/.idea", "**/obj/**/*", "**/.vs/**/*", "**/bin/**/*"],
		webStorm: ["**/obj/**/*", "**/bin/**/*", "**/*.njsproj", "**/*.njsproj.user", "**/.vs/**/*"]
	};
	
	//Pick files base on IDE
	if (ideChoice === ide.webStorm) {
		globs.push({root: "./Template/", pattern: "**/*.*", ignore: ideIgnore.webStorm})
	} else if (ideChoice === ide.vs2017) {
		globs.push({root: "./Template/", pattern: "**/*.*", ignore: ideIgnore.vs2017});
	} else {
		globs.push({
			root: "./Template/",
			pattern: "**/*.*",
			ignore: ideIgnore.vs2017.concat(ideIgnore.webStorm)
		})
	}
	//Ask for documentation
	let docQuestions: DocUsage = {license: false, git: false, readme: true, semVer: false, contributing: false};
	let docs: Documentation = await autoInquirer.InquireDocument(docQuestions);
	
	let flags: any = {
		xunit: "xUnit",
		nunit: "nUnit"
	};
	
	let unittest = await autoInquirer.InquirePredicate("Do you want unit test?");
	if (unittest) {
		flags = await autoInquirer.InquireAsList(flags, "Which unit test framewok do you want to use?");
	} else {
		flags.nunit = false;
		flags.xunit = false;
	}
	
	let guid: string[] = ["C83ED6E8-08FB-431A-8F35-BD6B5F091208",
		"07737E5D-2993-4F9C-8CD5-945DD4E54748"];
	if (flags.nunit) guid.push("6445B8C9-EE9D-4375-8382-E819F15443E3");
	if (flags.xunit) guid.push("D36DD335-3428-4CFD-8DB2-7C474081BAF0");
	
	return {
		globs: globs,
		flags: flags,
		guid: guid,
		docs: docs,
		comments: ["#", "//"]
	} as Cyan;
}
