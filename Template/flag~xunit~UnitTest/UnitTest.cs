using System;
using Xunit;
using library;

namespace test
{
    public class UnitTest
    {
        [Fact]
        public void String_Reverse_should_reverse_the_string()
        {
            Assert.Equal("eennirik", "kirinnee".Invert());
        }
    }
}
