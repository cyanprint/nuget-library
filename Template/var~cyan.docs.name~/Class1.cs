﻿using System;
using System.Linq;

namespace library
{
    public static class Helper
    {
        public static string Invert(this string x)
        {
            return string.Join("", x.Reverse());
        }
    }
}
