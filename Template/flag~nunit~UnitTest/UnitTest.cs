using NUnit.Framework;
using library;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test_Invert_String()
        {
            Assert.AreEqual("eennirik", "kirinnee".Invert());
        }
    }
}