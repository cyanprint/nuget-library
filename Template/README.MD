# var~cyan.docs.name~

var~cyan.docs.description~

# Getting Started

Install via .NET CLI
```bash
$ dotnet add package var~cyan.docs.name~
```

or 

Install via NuGet Package Manager
```powershell
PM> Install-Package var~cyan.docs.name~
```

if~cyan.docs.contributing~	
## Contributing
Please read [CONTRIBUTING.md](CONTRIBUTING.MD) for details on our code of conduct, and the process for submitting pull requests to us.
end~cyan.docs.contributing~	

if~cyan.docs.semver~
## Versioning 
We use [SemVer](https://semver.org/) for versioning. For the versions available, see the tags on this repository.
end~cyan.docs.semver~

## Authors
* [var~cyan.docs.author~](mailto:var~cyan.docs.email~) 

if~cyan.docs.license~
## License
This project is licensed under var~cyan.docs.license~ - see the [LICENSE.md](LICENSE.MD) file for details
end~cyan.docs.license~